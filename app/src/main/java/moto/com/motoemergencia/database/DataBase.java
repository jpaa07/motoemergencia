package moto.com.motoemergencia.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.File;

public class DataBase {

    public static boolean doesDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile.exists();
    }

    public boolean validateIfTableHasData(SQLiteDatabase myDatabase,String tableName){
        Cursor c = myDatabase.rawQuery("SELECT * FROM " + tableName,null);
        return c.moveToFirst();
    }

    public static final String CREATEUSERTABLE = "CREATE TABLE IF NOT EXISTS `users` (\n" +
            "  `nombre` VARCHAR(45) NULL,\n" +
            "  `apellidos` VARCHAR(45) NULL,\n" +
            "  `contraseña` VARCHAR(45) NULL,\n" +
            "  `fecha_nacimiento` VARCHAR(45) NULL,\n" +
            "  `ciudad_nacimiento` VARCHAR(45) NULL,\n" +
            "  `email` VARCHAR(45) NULL,\n" +
            "  `direccion` VARCHAR(45) NULL,\n" +
            "  `tipo_sangre` VARCHAR(45) NULL,\n" +
            "  `notas` VARCHAR(1000) NULL);";

    public static final String CREATECONTACTSTABLE = "CREATE TABLE IF NOT EXISTS `contacts` (\n" +
            "  `nombre` VARCHAR(45) NULL,\n" +
            "  `celular` VARCHAR(45) NULL);";

    public void addUser(SQLiteDatabase myDatabase, String nombre,String apellidos,String contraseña,String fechaNacimiento,String email,String direccion,String tipoSangre,String notas){
        String createUser = "INSERT INTO users (nombre," +
                "apellidos," +
                "contraseña," +
                "fecha_nacimiento," +
                "email," +
                "direccion," +
                "tipo_sangre," +
                "notas) " +
                "VALUES ('"+nombre+"'," +
                "'"+apellidos+"'," +
                "'"+contraseña+"'," +
                "'"+fechaNacimiento+"'," +
                "'"+email+"'," +
                "'"+direccion+"'," +
                "'"+tipoSangre+"'," +
                "'"+notas+"')";
        myDatabase.execSQL(createUser);
    }

    public void addContact(SQLiteDatabase myDataBase,String nombre,String celular) {
        String createContact = "INSERT INTO contacts (nombre,celular) " +
                "VALUES ('"+nombre+"','"+celular+"')";
        myDataBase.execSQL(createContact);
    }
}
