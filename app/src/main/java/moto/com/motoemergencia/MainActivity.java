package moto.com.motoemergencia;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import moto.com.motoemergencia.database.DataBase;

public class MainActivity extends AppCompatActivity {

    private DataBase dataBase = new DataBase();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SQLiteDatabase myDatabase = this.openOrCreateDatabase("data",MODE_PRIVATE,null);
        myDatabase.execSQL(DataBase.CREATEUSERTABLE);
        myDatabase.execSQL(DataBase.CREATECONTACTSTABLE);
        myDatabase.execSQL("DELETE FROM users");
        if(dataBase.validateIfTableHasData(myDatabase,"users")){
                setContentView(R.layout.activity_login_as);
        } else {
                setContentView(R.layout.activity_main);
        }
    }

    public void goToAddUser(View view){
        Intent intent = new Intent(getApplicationContext(),UserHistoryActivity.class);
        startActivity(intent);
    }
}
