package moto.com.motoemergencia;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import moto.com.motoemergencia.database.DataBase;

public class AddContactActivity extends AppCompatActivity {

    DataBase dataBase = new DataBase();

    ArrayList<String> contacts = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    ListView listContact;
    Button btnAddContacts;

    int numberOfContacts = 0;

    SQLiteDatabase myDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        myDatabase = this.openOrCreateDatabase("data",MODE_PRIVATE,null);
        btnAddContacts = (Button) findViewById(R.id.btnAddContacts);
        btnAddContacts.setVisibility(View.INVISIBLE);
        contacts.add("Agregar contacto nuevo");
        listContact = (ListView) findViewById(R.id.listContact);
        arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,contacts);
        listContact.setAdapter(arrayAdapter);
        listContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(((AppCompatTextView) view).getText().equals("Agregar contacto nuevo") && (numberOfContacts<5)){
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                    startActivityForResult(intent, 1);
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Uri contactData = data.getData();
                Cursor cursor = managedQuery(contactData, null, null, null, null);
                cursor.moveToFirst();
                String number = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
                String name = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                contacts.add(name);
                arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,contacts);
                listContact.setAdapter(arrayAdapter);
                dataBase.addContact(myDatabase,name,number);
                numberOfContacts++;
                if(numberOfContacts>0){
                    btnAddContacts.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void goToLoginAs(View view){
        Intent intent = new Intent(getApplicationContext(),LoginAsActivity.class);
        startActivity(intent);
        Toast.makeText(this, "Se creo perfil correctamente", Toast.LENGTH_SHORT).show();
    }
}
