package moto.com.motoemergencia;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import moto.com.motoemergencia.database.DataBase;

public class UserHistoryActivity extends AppCompatActivity {

    DataBase dataBase = new DataBase();

    EditText txtUserName;
    EditText txtUserLastName;
    EditText txtNewPassword;
    EditText txtUserBirthday;
    EditText txtUserEPS;
    EditText txtUserEmail;
    EditText txtUserAddress;
    EditText txtNotes;
    Spinner bloodTypes;

    ArrayList<EditText> fields = new ArrayList<>();

    SQLiteDatabase myDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_history);
        myDatabase = this.openOrCreateDatabase("data",MODE_PRIVATE,null);
        txtUserName = (EditText) findViewById(R.id.txtUserName);
        txtUserLastName = (EditText) findViewById(R.id.txtUserLast);
        txtNewPassword = (EditText) findViewById(R.id.txtNewPassword);
        txtUserBirthday = (EditText) findViewById(R.id.txtBirthday);
        txtUserEPS = (EditText) findViewById(R.id.txtEPS);
        txtUserEmail = (EditText) findViewById(R.id.txtUserEmail);
        txtUserAddress = (EditText) findViewById(R.id.txtUserAddress);
        txtNotes = (EditText) findViewById(R.id.txtAdditionalNotes);
        bloodTypes = (Spinner) findViewById(R.id.spnBloodType);
    }

    public void goToSelectContacts(View buttonContact){
        fields.clear();
        fields.add(txtUserName);
        fields.add(txtUserLastName);
        fields.add(txtNewPassword);
        fields.add(txtUserBirthday);
        fields.add(txtUserEPS);
        fields.add(txtUserAddress);
        fields.add(txtUserEmail);
        fields.add(txtNotes);
        cleanStyleFields(fields);
        if(validateFields(fields)){
            Intent intent = new Intent(getApplicationContext(),AddContactActivity.class);
            dataBase.addUser(myDatabase,txtUserName.getText().toString(),txtUserLastName.getText().toString(),
                    txtNewPassword.getText().toString(),txtUserBirthday.getText().toString(),
                    txtUserEmail.getText().toString(),txtUserAddress.getText().toString(),
                    bloodTypes.getTransitionName(),txtNotes.getText().toString());
            startActivity(intent);
        }
    }

    private void generateAlertToast(){
        Toast.makeText(this, "Falta diligenciar algun campo", Toast.LENGTH_SHORT).show();
    }

    private boolean validateFields(ArrayList<EditText> fields){
        for (EditText field : fields){
            if(field.getText().toString().isEmpty()){
                field.setBackgroundColor(Color.RED);
                field.setTextColor(Color.WHITE);
                field.setHintTextColor(Color.WHITE);
                generateAlertToast();
                return false;
            }
        }

        if ( bloodTypes.getSelectedItemPosition() == 0){
            bloodTypes.setBackgroundColor(Color.RED);
            generateAlertToast();
            return false;
        }
        return true;
    }

    private void cleanStyleFields(ArrayList<EditText> fields){
        for (EditText field: fields){
            field.setBackgroundColor(Color.WHITE);
            field.setTextColor(Color.BLACK);
            field.setHintTextColor(Color.GRAY);
            field.setLinkTextColor(Color.GRAY);
        }
    }
}
